package com.demo.hello.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app")
public class HomeController {
	
	@RequestMapping("/home")
	public String home() {
		return "This is home page";
	}

}
